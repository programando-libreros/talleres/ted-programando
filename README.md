# Programando LIBREros -- TED

Taller de Edición Digital interno.

## Notas

Visita el [pad](https://pad.programando.li/programando-ted-interno).

## Bibliografía

Libros para el taller:

* [_How to Design Programs_](https://htdp.org/)
* [_The Well Grounded Rubyist_](http://gen.lib.rus.ec/book/index.php?md5=A5C884FACC5623CE320B1D947B6BDC92)
